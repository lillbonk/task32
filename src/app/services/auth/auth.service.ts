import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
  
})

export class UserService {

 
  constructor(private http: HttpClient) { }


  register(user): Promise<any>{
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/register',
      {
      user: { ...user },
    }).toPromise()

  }

}

@Injectable({
  providedIn: 'root'
  
})

export class AuthService {

  constructor(private http: HttpClient) { }

/**
 * attempt to login with given user
 * @param user 
 * @returns boolean
 * 
 */
  login(user: any): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/login', 
    {
      user: {...user},

  }).toPromise()

   }

}



