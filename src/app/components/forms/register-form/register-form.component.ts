import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import { UserService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit{

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('',
    [ Validators.required,
      Validators.minLength(5),
      Validators.email
    ]),
    password: new FormControl('' , 
    [ Validators.required,
      Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
    ]),
    confirmPassword: new FormControl('', 
    [Validators.required,
      Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
    ])
  })


  constructor(private userService: UserService , private router: Router){}

  get username() {
    return this.registerForm.get('username');
  };
  get password() {
    return this.registerForm.get('password');
  };
  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  };
  


 async onRegisterClick() {


  try {
  

    if (this.password.value !== this.confirmPassword.value){
      return alert('Passwords do not match. Try again.')
    }
    // exec the register fucntion from the service
   
    const result: any = await this.userService.register(this.registerForm.value);

    console.log(result);
    if (result){
      this.router.navigateByUrl('/dashboard');
    }
    

    
    
  } catch (e) {
    console.error(e);
    
    
  }
    
  }

  ngOnInit(): void {
  }  


}
