import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent  implements OnInit {

loginForm: FormGroup = new FormGroup({
  username: new FormControl('', 
  [ Validators.required,
    Validators.minLength(5),
    Validators.email
  ]),
  password: new FormControl('', 
  [ Validators.required,
    Validators.minLength(6)

  ])

})

constructor(private authService: AuthService, private router: Router) { }

get username() {
  return this.loginForm.get('username')
}
get password() {
  return this.loginForm.get('password')
}

ngOnInit(): void {
}


 async onLoginClick(){


      const success: any = await this.authService.login(this.loginForm.value);
      console.log(success);
      

      if (success) {
        this.router.navigateByUrl('/dashboard');
      } else {
        alert('NEIN, invlid username or password!')
      }
      
      
  }

}

